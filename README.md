# Devuanbox

Script para instalar Interfaz grafica con openbox en Devuan


Recuerda volverlo ejecuable con `chmod +x install`

Ejecutalo con:

`./install`

---

En caso tengas portatil y necesites funcionamiento del touchpad y brillo
elige la 2da opcion "Devuanbox Laptop"

---
Algunos Paquetes que contiene:

Panel: `Tint2`

Gestor de ventanas: `Openbox`

Navegador: `Firefox-ESR`

Gestor de Archivos: `Thunar`

Network Manager: `wicd-curses`(se ejecuta desde terminal)

Terminal: `Sakura`

Editor de Textos: `Geany`

Reproductor Multimedia: `MPV`
